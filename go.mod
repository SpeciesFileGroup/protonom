module gitlab.com/SpeciesFileGroup/protonom

require (
	github.com/golang/protobuf v1.2.1-0.20190109072247-347cf4a86c1c
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.4-0.20190109003409-7547e83b2d85
	github.com/spf13/viper v1.3.1
	gitlab.com/gogna/gnparser v0.7.4
	golang.org/x/net v0.0.0-20190125091013-d26f9f9a57f3
	google.golang.org/grpc v1.19.0
)

package grpc

import (
	"fmt"
	"io"
	"log"
	"net"

	"gitlab.com/SpeciesFileGroup/protonom"
	"gitlab.com/SpeciesFileGroup/protonom/proto"
	context "golang.org/x/net/context"
	"google.golang.org/grpc"
)

type harvesterServer struct {
}

func (harvesterServer) Ver(ctx context.Context,
	v *proto.Void) (*proto.Version, error) {
	return &proto.Version{Value: protonom.Version, Build: protonom.Build}, nil
}

func (harv harvesterServer) Import(stream proto.Harvester_ImportServer) error {
	importNum := 0
	for {
		ii, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		switch ii.Content.(type) {
		case *proto.ImportInput_Taxon:
			importNum++
			st := &proto.ImportStats{ImportedTaxa: int32(importNum)}
			stream.Send(st)
		case *proto.ImportInput_NameElement:
		}

	}
}

func Run(port int) {
	harv := harvesterServer{}
	srv := grpc.NewServer()
	proto.RegisterHarvesterServer(srv, harv)
	portVal := fmt.Sprintf(":%d", port)
	l, err := net.Listen("tcp", portVal)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", portVal, err)
	}
	log.Fatal(srv.Serve(l))
}

GOCMD = go
GOBUILD = $(GOCMD) build
GOINSTALL = $(GOCMD) install
GOCLEAN = $(GOCMD) clean
GOGET = $(GOCMD) get
FLAG_MODULE = GO111MODULE=on
FLAGS_SHARED = $(FLAG_MODULE) CGO_ENABLED=0 GOARCH=amd64
FLAGS_LINUX = $(FLAGS_SHARED) GOOS=linux
FLAGS_MAC = $(FLAGS_SHARED) GOOS=darwin
FLAGS_WIN = $(FLAGS_SHARED) GOOS=windows

VERSION = $(shell git describe --tags)
VER = $(shell git describe --tags --abbrev=0)
DATE = $(shell date -u '+%Y-%m-%d_%H:%M:%S%Z')

all: install

version:
	echo "package protonom" > version.go
	echo "" >> version.go
	echo "const Version = \"$(VERSION)"\" >> version.go
	echo "const Build = \"$(DATE)\"" >> version.go

deps:
	$(FLAG_MODULE) $(GOGET) github.com/golang/protobuf/protoc-gen-go@347cf4a; \
	$(FLAG_MODULE) $(GOGET) github.com/spf13/cobra/cobra@7547e83; \

install: version grpc
	cd protonom; \
	$(GOCLEAN); \
	$(FLAGS_SHARED) $(GOINSTALL)

.PHONY:grpc
grpc:
	cd proto; \
	$(FLAG_MODULE) protoc -I . ./import.proto --go_out=plugins=grpc:.;
